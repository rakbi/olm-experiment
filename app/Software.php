<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Software extends Model
{
	protected $fillable = ["name"];
    protected $table = 'softwares';
	
    public function devices() {
    	return $this->belongsToMany(Device::class,"experiments");
    }
}
